﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace Module3_2
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = ParseAndValidateIntegerNumber(input);
            return true;
        }

        public int ParseAndValidateIntegerNumber(string source)
        {
            int num;
            if (source == "0") return 0;
            Int32.TryParse(source, out num);
            if (num == 0 || num < 0)
            {
                source = Console.ReadLine();
                return ParseAndValidateIntegerNumber(source);
            }
            return num;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int first = 0, second = 1;
            int[] fibonacci = new int[n];
            if (n == 0) return fibonacci;
            fibonacci[0] = first;
            if (n == 1) return fibonacci;
            fibonacci[1] = second;
            for (int i = 2; i < n; i++)
            {
                fibonacci[i] = second + first;
                first = second;
                second = fibonacci[i];
            }
            return fibonacci;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int allnum = sourceNumber;
            if (sourceNumber < 0) allnum *= -1;
            string numb = allnum.ToString();
            char[] cifr = numb.ToCharArray();
            Array.Reverse(cifr);
            string reverse = new string(cifr);
            Int32.TryParse(reverse, out allnum);
            if (sourceNumber < 0) allnum *= -1;
            return allnum;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] randomarray = { };
            if (size >= 0)
            {
                randomarray = new int[size];
                if (size < 0) return randomarray;
                Random rnd = new Random();
                for (int i = 0; i < size; i++)
                {
                    randomarray[i] = rnd.Next(-10, 10);
                }
                return randomarray;
            }
            else return randomarray;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
                source[i] *= -1;
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] randomarray = { };
            if (size >= 0)
            {
                randomarray = new int[size];
                if (size < 0) return randomarray;
                Random rnd = new Random();
                for (int i = 0; i < size; i++)
                {
                    randomarray[i] = rnd.Next(-10, 10);
                }
                return randomarray;
            }else return randomarray;
            
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> elements = new List<int>();
            int f = 0;
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    elements.Insert(f, source[i]);
                    ++f;
                }
            }
            return elements;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] matrix = { };
            if (size < 0) return matrix;
            int str = 0, vert = 0, x = 1, y = 0, change = 0, enter = size;
            matrix = new int[size, size];
            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[str, vert] = i + 1;
                if (--enter == 0)
                {
                    enter = size * (change % 2) + size * ((change + 1) % 2) - (change / 2 - 1) - 2;
                    int temp = x;
                    x = -y;
                    y = temp;
                    change++;
                }
                vert += x;
                str += y;
            }
            return matrix;
        }
    }
}
